#!/bin/bash
git clone https://gitlab.com/maksimvlad7/2nd_awesome-sauce.git -b los-21-rebase device/realme/nashc
git clone https://github.com/Killerpac/android_vendor_realme_nashc.git -b master vendor/realme/nashc
git clone https://github.com/PQEnablers-Devices/android_device_mediatek_sepolicy_vndr.git -b lineage-21 device/mediatek/sepolicy_vndr
git clone https://github.com/nashc-dev/android_hardware_mediatek.git -b lineage-20 hardware/mediatek
git clone https://github.com/nashc-dev/android_hardware_mediatek_wlan.git -b lineage-20 hardware/mediatek/wlan
git clone https://github.com/LineageOS/android_hardware_oplus.git -b lineage-21 hardware/oplus
git clone https://gitlab.com/maksimvlad7/kernel_realme_mt6785.git -b vlad-experimental kernel/realme/mt6785

echo "Nashc source complete, now edit some stuff (device.mk, ..._nashc.mk etc) for rom compatibility"
echo "And some stuff in frameworks/base, hardware/mediatek, like revert gnss drop, udfps dim, arraymap to hashmap (by choice ofc)"
